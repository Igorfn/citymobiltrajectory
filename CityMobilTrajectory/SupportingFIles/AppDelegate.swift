//
//  AppDelegate.swift
//  CityMobilTrajectory
//
//  Created by Igor Feshchun on 09/10/2019.
//  Copyright © 2019 IgorFeshchun. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let variantA = MovementViewController()
        variantA.movement = LinearMovement()
        let variantB = MovementViewController()
        variantB.movement = CurveMovement()
        
        variantA.tabBarItem.title = "Linear"
        variantB.tabBarItem.title = "Curve"
        let tabbar = TabBarController.init()
        tabbar.setViewControllers([variantA, variantB], animated: false)
        window = UIWindow.init(frame: UIScreen.main.bounds)
        window?.rootViewController = tabbar
        window?.makeKeyAndVisible()
        
        return true
    }
}

