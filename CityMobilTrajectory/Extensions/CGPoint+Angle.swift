//
//  CGPoint+Angle.swift
//  CityMobilTrajectory
//
//  Created by Igor Feshchun on 14/10/2019.
//  Copyright © 2019 IgorFeshchun. All rights reserved.
//

import UIKit

extension CGPoint {
    func angle(to point: CGPoint) -> CGFloat {
        let center = CGPoint(x: point.x - self.x, y: point.y - self.y)
        let radians = atan2(center.y, center.x).convertAtan2ResultToCorrectRange().toRadians()
        return radians
    }
}

extension CGFloat {
    func toRadians() -> CGFloat {
        return self * .pi / 180
    }
    
    func toDegrees() -> CGFloat {
        return self * 180 / .pi
    }
    
    func convertAtan2ResultToCorrectRange() -> CGFloat {
        var degrees = self.toDegrees()
        
        if degrees < 0 {
            degrees += 360
        }
         
        return degrees
    }
}


extension CGPoint {
    func offsetBy(dx: CGFloat, dy: CGFloat) -> CGPoint {
        return CGPoint.init(x: self.x + dx, y: self.y + dy)
    }
}
