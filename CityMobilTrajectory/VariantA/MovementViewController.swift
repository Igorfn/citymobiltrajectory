//
//  MovementViewController.swift
//  CityMobilTrajectory
//
//  Created by Igor Feshchun on 13/10/2019.
//  Copyright © 2019 IgorFeshchun. All rights reserved.
//

import UIKit
import SnapKit

final class MovementViewController: UIViewController {
    private lazy var movementView = MovementView()
    
    var movement: MovementProtocol!
    
    
    override func loadView() {
        
        self.view = self.movementView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.movement.delegate = self
        self.view.backgroundColor = .white
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        let position = touch.location(in: self.view)
        self.movement.move(to: position)
    }
}

extension MovementViewController: MovementViewProviderProtocol {
    func viewForMovement() -> UIView {
        return self.movementView.carView
    }
}
