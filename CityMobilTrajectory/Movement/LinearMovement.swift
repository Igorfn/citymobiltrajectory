//
//  LinearMovement.swift
//  CityMobilTrajectory
//
//  Created by Igor Feshchun on 14/10/2019.
//  Copyright © 2019 IgorFeshchun. All rights reserved.
//

import UIKit

final class LinearMovement: MovementProtocol {
    private(set) var isAnimating: Bool = false

    private let rotationCoef: CGFloat = 0.174533 // 10°
    private let rotationCoefDuration: CGFloat = 0.09
    private let movementCoef: CGFloat = 25
    private let movementCoefDuration: CGFloat = 0.07
    
    weak var delegate: MovementViewProviderProtocol?
    
    private var view: UIView {
        return self.delegate?.viewForMovement() ?? UIView.init()
    }
    
    func move(to point: CGPoint) {
        guard !self.isAnimating else {
            return
        }
        
        self.isAnimating.toggle()
        
        let (rotationAngle, rotationDuration) = self.calculateRotationParameters(for: point)
        let moveDuration = self.calculateMovementDuration(for: point)
        let rotation = UIViewPropertyAnimator.init(duration: rotationDuration,
                                                   curve: .linear,
                                                   animations: { self.view.transform = CGAffineTransform.init(rotationAngle: rotationAngle) })
        
        let move = UIViewPropertyAnimator.init(duration: moveDuration,
                                               curve: .easeInOut,
                                               animations: { self.view.center = point })
        
        rotation.addCompletion { _ in
            move.startAnimation()
        }
        
        move.addCompletion { _ in
            self.isAnimating.toggle()
        }
        
        rotation.startAnimation()
    }
    
    private func calculateRotationParameters(for point: CGPoint) -> (CGFloat, Double) {
        let startAngle = atan2(self.view.transform.b, self.view.transform.a).convertAtan2ResultToCorrectRange().toRadians()
        let rotationAngle = self.view.center.angle(to: point)
        let angleDiff = startAngle - rotationAngle

        let rotationDuration = Double(abs(angleDiff) / self.rotationCoef * self.rotationCoefDuration)
        return (rotationAngle, rotationDuration)
    }
    
    private func calculateMovementDuration(for point: CGPoint) -> Double {
        let distance = hypot(self.view.center.x - point.x, self.view.center.y - point.y)
        let moveDuration = Double(distance / self.movementCoef * self.movementCoefDuration)
        return moveDuration
    }
}
