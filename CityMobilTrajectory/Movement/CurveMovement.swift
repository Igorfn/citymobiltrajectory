//
//  CurveMovement.swift
//  CityMobilTrajectory
//
//  Created by Igor Feshchun on 14/10/2019.
//  Copyright © 2019 IgorFeshchun. All rights reserved.
//

import UIKit

final class CurveMovement: NSObject, MovementProtocol {

    private(set) var isAnimating: Bool = false
    
    private lazy var displayLink: CADisplayLink = {
        let displayLink = CADisplayLink.init(target: self, selector: #selector(self.handleAnimation))
        displayLink.isPaused = true
        displayLink.add(to: .main, forMode: .common)
        return displayLink
    }()
    private let cubicAlgorithm = CubicCurveAlgorithm()
    weak var delegate: MovementViewProviderProtocol?
    
    private var view: UIView {
        return self.delegate?.viewForMovement() ?? UIView.init()
    }
    
    private var previousPosition: CGPoint {
        get {
            return self.point
        }
        
        set {
            self.point = newValue
        }
    }
    
    private var point = CGPoint.zero
    
    
    @objc private func handleAnimation() {
        guard let position = self.view.layer.presentation()?.position else {
            return
        }
        
        guard abs(position.x - self.previousPosition.y) > 0.01 ||
            abs(position.y - self.previousPosition.y) > 0.01 else {
            return
        }
        
        let angle = self.previousPosition.angle(to: position)
        self.view.transform = CGAffineTransform.init(rotationAngle: angle)
        self.previousPosition = position
    }
    
    func move(to point: CGPoint) {
        guard !self.isAnimating else {
            return
        }
        
        self.previousPosition = self.view.center
        self.displayLink.isPaused = false

        var points: [CGPoint] = [self.view.center]
        
        let startAngle = atan2(self.view.transform.b, self.view.transform.a).convertAtan2ResultToCorrectRange()
        let angle = self.view.center.angle(to: point).toDegrees()
        let angleDiff = startAngle - angle
        let isClockwise = angle > startAngle
        let sign: CGFloat = isClockwise ? 1 : -1
        
        if abs(angleDiff) < 10 {
            
        } else if abs(angleDiff) > 180 {
            let first = self.view.center.offsetBy(dx: sign * 50, dy: -sign * 50)
            let second = self.view.center.offsetBy(dx: sign * 100, dy: 0)
            
            points.append(contentsOf: [first, second])
        } else {
            
            let mid = self.view.center.offsetBy(dx: sign * self.view.frame.width / 4, dy: -1 * self.view.frame.height)
            points.append(mid)
        }
        
        points.append(point)
        
        let controlPoints = self.cubicAlgorithm.controlPointsFromPoints(dataPoints: points)
        
        let path = UIBezierPath.init()
        
        for i in 0..<points.count {
            let point = points[i]

            if i==0 {
                path.move(to: point)
            } else {
                let segment = controlPoints[i-1]
                path.addCurve(to: point, controlPoint1: segment.controlPoint1, controlPoint2: segment.controlPoint2)
            }
        }
        
        let position = CAKeyframeAnimation(keyPath: "position")
        position.path = path.cgPath
        position.duration = 2.0
        position.fillMode = .forwards
        position.repeatCount = 0
        position.isRemovedOnCompletion = false
        position.delegate = self
        
        self.view.layer.add(position, forKey: "animate movement along path")
    }
}

extension CurveMovement: CAAnimationDelegate {
    func animationDidStart(_ anim: CAAnimation) {
        if anim == self.view.layer.animation(forKey: "animate movement along path") {
            self.isAnimating.toggle()
        }
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        guard anim == self.view.layer.animation(forKey: "animate movement along path"),
            let presentationLayer = self.view.layer.presentation() else {
                return
        }
        
        self.displayLink.isPaused = true
        self.view.layer.position = presentationLayer.position
        self.view.layer.removeAnimation(forKey: "animate movement along path")
        self.isAnimating.toggle()
    }
}

