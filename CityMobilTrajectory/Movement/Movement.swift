//
//  Movement.swift
//  CityMobilTrajectory
//
//  Created by Igor Feshchun on 14/10/2019.
//  Copyright © 2019 IgorFeshchun. All rights reserved.
//

import UIKit

protocol MovementViewProviderProtocol: class {
    func viewForMovement() -> UIView
}

protocol MovementProtocol {
    func move(to point: CGPoint)
    var delegate: MovementViewProviderProtocol? { get set }
}
