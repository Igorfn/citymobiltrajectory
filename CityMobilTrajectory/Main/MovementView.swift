//
//  MovementView.swift
//  CityMobilTrajectory
//
//  Created by Igor Feshchun on 12/10/2019.
//  Copyright © 2019 IgorFeshchun. All rights reserved.
//

import UIKit
import SnapKit

final class MovementView: UIView {
    
    lazy var resetButton = UIButton.init(type: .system)
    
    lazy var carView: UIImageView = {
        let view = UIImageView()
        view.image = UIImage.init(named: "car")
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.snp.makeConstraints {
            $0.width.equalTo(93)
            $0.height.equalTo(66)
        }
        
        view.transform = CGAffineTransform.init(rotationAngle: 3 * .pi / 2)
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.render()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.render()
    }
    
    private func render() {
        self.addSubview(self.carView)
        self.addSubview(self.resetButton)
        self.resetButton.setTitle("Reset", for: .normal)
        self.resetButton.addTarget(self, action: #selector(self.reset(_:)), for: .touchUpInside)
        
        self.resetButton.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.topMargin.equalToSuperview().offset(10)
            $0.height.equalTo(44)
            $0.width.equalTo(64)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.carView.center = self.center
    }
    
    @objc private func reset(_ sender: UIButton) {
        self.carView.center = self.center
        self.carView.transform = CGAffineTransform.init(rotationAngle: 3 * .pi / 2)
    }
}
